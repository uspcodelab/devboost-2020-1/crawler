import json

f = open("requisites.json", "r")
reqDict = json.load(f)
f.close()

idPreRequesite = 1
for key in reqDict.keys():
    idSubject = reqDict[key]["code_id"]
    preRequisites = reqDict[key]["pre_requisites"]
    if len(preRequisites[0]) != 0:
        for preReq in preRequisites:
            print("insert into prerequisitos(forca, codigo_curso) values (1, '45052');")
            print("insert into distemprereq(disciplina_id, prerequisito_id) "
                  "values (%d, %d);" % (idSubject, idPreRequesite))
            for preReqSubject in preReq:
                idPreReqSub = reqDict[preReqSubject]["code_id"]
                print("insert into prereqcompdis(disciplina_id, prerequisito_id) "
                      "values (%d, %d);" % (idPreReqSub, idPreRequesite))
            idPreRequesite += 1
