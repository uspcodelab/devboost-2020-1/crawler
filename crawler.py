from urllib.request import urlopen
from bs4 import BeautifulSoup

CODES = ['MAC0101', 'MAC0105', 'MAC0110', 'MAC0329', 'MAT0112', 'MAT2453', 'MAC0121', 'MAC0216', 'MAC0239', 'MAE0119', 'MAT0122',  'MAT2454', 'MAC0102', 'MAC0209', 'MAC0210', 'MAC0323', 'MAT0236', 'MAC0316', 'MAC0338', 'MAC0350', 'MAC0422', 'FLC0474', 'MAC0499', 'GMG0630', 'ACH2023', 'ACH2033', 'MAC0213', 'MAC0214', 'MAC0215', 'CJE0642', 'IOF0115', 'MAC0327', 'MAC0337', 'MAC0453', 'MAE0217', 'MAE0228', 'MAE0399', 'MAP2001', 'MAT0206', 'MAT0222', 'MAT0223', 'MAT0264', '0323100', '0440620', '4302112', '4302401', 'MAC0218', 'MAC0219', 'MAC0242', 'MAC0300', 'MAC0341', 'MAC0474', 'MAE0221', 'MAE0224', 'MAE0515', 'MAT0265', 'MAT0311', 'PCS3549', 'QBQ0126', 'QBQ0230', 'QBQ0250', 'QBQ1252', 'QBQ2457', 'QBQ2503', 'QBQ2507', 'QBQ2508', 'QBQ2509', 'AUH2803', 'AUP1301', 'AUP2409', 'IOF0255', 'IOF0265', 'MAC0315', 'MAC0320', 'MAC0328', 'MAC0345', 'MAC0346', 'MAC0351', 'MAC0352', 'MAC0385', 'MAC0431', 'MAC0444', 'MAC0469', 'MAE0311', 'MAE0312', 'MAE0532', 'MAP2210', 'MAT0225', 'MAT0364', 'PCS2305', 'QBQ0102', 'QBQ0104', 'QBQ0106', 'QBQ0116', 'QBQ0204', 'QBQ0317', 'QBQ1354', 'QBQ2502', 'QBQ2505', 'ACH2096', 'ACH2106', 'MAC0317', 'MAC0318', 'MAC0325', 'MAC0331', 'MAC0332', 'MAC0339', 'MAC0340', 'MAC0343',
         'MAC0344', 'MAC0375', 'MAC0412', 'MAC0414', 'MAC0417', 'MAC0420', 'MAC0425', 'MAC0426', 'MAC0441', 'MAC0448', 'MAC0450', 'MAC0470', 'MAC0471', 'MAC0473', 'MAC0475', 'MAE0325', 'MAE0328', 'MAP0313', 'MAP2220', 'MAT0234', 'MAT0350', 'PCS3345', 'ACH2007', 'ACH2066', 'ACH2067', 'ACH2076', 'ACH2077', 'ACH2087', 'ACH2107', 'ACH2117', 'ACH2127', 'ACH2137', 'MAC0322', 'MAC0333', 'MAC0336', 'MAC0413', 'MAC0416', 'MAC0427', 'MAC0430', 'MAC0432', 'MAC0436', 'MAC0438', 'MAC0439', 'MAC0446', 'MAC0451', 'MAC0452', 'MAC0456', 'MAC0459', 'MAC0464', 'MAC0466', 'MAC0467', 'MAC0468', 'MAC0472', 'MAC0546', 'MAC0690', 'MAC0691', 'MAC0775', 'MAE0314', 'MAE0315', 'MAE0326', 'MAP2310', 'MAT0330', 'MAT0359', 'PCS0216', 'PCS3529', 'ACH2028', 'ACH2037', 'ACH2038', 'ACH2048', 'ACH2068', 'ACH2078', 'ACH2086', 'ACH2098', 'ACH2108', 'ACH2118', 'MAC0319', 'MAC0326', 'MAC0419', 'MAC0424', 'MAC0434', 'MAC0435', 'MAC0447', 'MAC0457', 'MAC0458', 'MAC0460', 'MAC0463', 'MAC0465', 'MAC0485', 'MAC0536', 'MAC0552', 'MAC0692', 'MAC0776', 'MAE0330', 'MAP2321', 'MAP2411', 'PCS0210', 'ACA0115', 'MAC0335', 'ACA0223', 'AGA0215', 'ACA0522', 'AGA0106', 'AGA0315', 'AGG0110', 'ACA0537', 'AGG0222', 'AGG0330', 'ACA0324', 'AGA0309', 'ACA0245']


def crawlSubjects():
    allSubjectsUrl = "https://uspdigital.usp.br/jupiterweb/listarGradeCurricular?codcg=45&codcur=45052&codhab=1&tipo=N"
    subjectUrl = "https://uspdigital.usp.br/jupiterweb/obterDisciplina?sgldis="
    jupiter = urlopen(allSubjectsUrl).read()
    soup = BeautifulSoup(jupiter, 'html.parser')
    subjects = soup.findAll("tr", {"class": "txt_verdana_8pt_gray"})
    codes = []
    for subject in subjects:
        subjectObj = {}
        soup = BeautifulSoup(str(subject), 'html.parser')
        subjectInfos = soup.findAll('td')
        if len(subjectInfos) >= 4:
            code = subjectInfos[0].text.split()[0]
            codes.append(code)
            name = subjectInfos[1].text.strip()
            class_credits = subjectInfos[2].text.split()[0]
            work_credits = subjectInfos[3].text.split()[0]
            print('CREATE (%s:Discipline {code: "%s", name: "%s", aula: "%s", trab: "%s"})'% (code.lower(), code, name, class_credits, work_credits))


def crawlReq(codes):
    subjects = {}
    codeIndex = 1
    for code in codes:
        reqUrl = "https://uspdigital.usp.br/jupiterweb/listarCursosRequisitos?coddis=" + code
        jupiter = urlopen(reqUrl).read()
        soup = BeautifulSoup(jupiter, 'html.parser')

        trs = soup.findAll("tr", {"valign": "top"})
        courseCode = '0'
        allRequisites = []
        requisite = []
        subjects[code] = {}
        subjects[code]["code_id"] = codeIndex
        group = 1
        for row in trs:
            courseArray = row.text.split()
            if len(courseArray) > 1 and courseArray[0] == 'Curso:':
                courseCode = courseArray[1]
                continue
            if courseCode == '45052':
                requisitesArray = row.text.split()
                if requisitesArray[0] != 'ou':
                    requisite.append(requisitesArray[0])
                    print('CREATE (%s)-[:DEPENDS_ON {strength: "hard", course: "bcc", group: %s}]->(%s)' % (code.lower(), group, requisitesArray[0].lower()))
                else:
                    group += 1
                    # allRequisites.append(requisite)
                    # requisite = []
        allRequisites.append(requisite)
        subjects[code]["pre_requisites"] = allRequisites
        codeIndex += 1

    # print(subjects)


crawlSubjects()
crawlReq(CODES)
